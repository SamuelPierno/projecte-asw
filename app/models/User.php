<?php

class User extends Eloquent {


	protected $table = 'users';

	// Add your validation rules here
	public static $rules = [
		'username' => 'required| unique:users',
		'email' => 'required| unique:users'
		'password' => 'required'
	];


	protected $hidden = ['password'];

	public function getAuthIdentfier(){
		return $this->getKey();
	}

	public function getAuthPassword(){
		return $this->password;
	}

	public function post(){
		return $this->hasMany('Post');
	}

}