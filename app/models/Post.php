<?php

class Post extends Eloquent {


	

	// Add your validation rules here
	public static $rules = [
		'tittle' => 'required| unique:posts',
		'body' => 'required',
		'type' => 'required| unique:posts'
	];

	
	public function creator()
	{

		return $this->belongsTo('User', 'created_by');

	}

	public function updater(){

		return $this->belongsTo('User', 'updated_by');

	}

}