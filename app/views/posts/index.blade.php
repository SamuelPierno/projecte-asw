@extends('layouts.admin')


@section('content')

@if($posts->count())
	<div class="col-md-12">
		<table class="table table-bordered table-striped">
			@foreach($posts as $p)
			<tr>
				<!-- <td>{{ $p->tittle }}</td> -->
				<td>{{HTML::link('blog/'.$p->slug, $p->tittle)}}</td>	
			</tr>
			@endforeach
		</table>
	</div>
@else
	<div class="alert alert-info col-md-4" style="margin-top: 15px">You currently have no posts</div>
@endif



@stop